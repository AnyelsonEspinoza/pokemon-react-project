import example from '../fixtures/example.json'

describe('Page Test', () => {
  it('Log in, visit profile, visit dashboard then logouts', function () {
    cy.visit('/')
    const { email, password } = example
    cy.get('input[name=email]').type(email)
    cy.get('input[name=password]').type(`${password}{enter}`)
    cy.url().should('include', '/dashboard')
    cy.get('button[name=avatar]').click()
    cy.get('a[name=profile]').click()
    cy.get('a[name=dashboard]').click()
    cy.get('a[name=logout]').click()
  })
})

describe('Page Test', () => {
  it('Logs in the page open a modal and close the modal', function () {
    cy.visit('/')
    const { email, password } = example
    cy.get('input[name=email]').type(email)
    cy.get('input[name=password]').type(`${password}{enter}`)
    cy.url().should('include', '/dashboard')
    cy.get('div[name=bulbasaur]').click()
    cy.get('button[name=closeModal]').click()
  })
})