import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { addUser } from "../redux/userSlice";
import { useForm } from "../hooks/useForm";
import { useNavigate } from "react-router-dom";
import { Button } from "../components/button";
import { Input } from "../components/input";
import  tw from 'twin.macro';
import Title  from "../components/title";
import Card from "../components/card";
import Alert from "../components/alert";
import Loading from "../components/loading";
import ErrorText from "../components/errorText";
import mewtwo from '../assets/mewtwo2.png';
import users from '../constants/users.json';

const Container = tw.div`
  bg-gradient-to-r from-purple-500 w-full h-screen flex md:flex-row sm:flex-col-reverse
`

const initialForm = {
  email: '',
  password: ''
};

const validationsForm = (form) => {
  let errors = {}
  const regexEmail = /^(\w+[/./-]?){1,}@[a-z]+[/.]\w{2,}$/;
  
  if (!form.email.trim()) {
    errors.email = 'El campo Email es requerido';
  } else if (!regexEmail.test(form.email.trim())) {
    errors.email = 'Escriba un email valido';
  }
  
  if (!form.password.trim()) {
    errors.password = 'El campo Contraseña es requerido'
  }
  return errors;
}

export default function Login() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  
  useEffect(() => {
    const token = localStorage.getItem('token');
    if (token) {
      navigate("/dashboard");
    }
  }, [navigate]);
  
  const handleLogin = async () => {
    handleLoading(true);
    await setTimeout(() => {
      const index = users.findIndex(({ email, password }) => email === form.email && password === form.password);
      if (index !== -1) {
        localStorage.setItem("token", users[index].token);
        dispatch(addUser(users[index]))
        navigate("/dashboard");
      } else {
        handleAlert();
        handleLoading(false);
      }
    }, 3000);
  };

  const { 
    form, 
    errors,
    alert,
    loading,
    handleChange,
    handleBlur,
    handleSubmit,
    handleAlert,
    handleLoading
  } = useForm(initialForm, validationsForm, handleLogin);
  
  return (
    <>
      <Container>
        <div className="md:w-1/2 sm:h-full flex flex-col items-center justify-center">
          <Title className="mt-5 font-pocket text-center text-white">Bienvenidos a la MonomaDex</Title>
          <img src={mewtwo} alt="mewtwo" className="sm:w-1/2 md:w-50"/>
        </div>
        <Card className="rounded-md">
          <Title>Iniciar sesión</Title>
          <form method="post" className="flex flex-col" onSubmit={handleSubmit}>
            <div className="mt-2">Correo:</div>
            <Input 
              type="email"
              name="email"
              placeholder="Email"
              value={form.email}
              onBlur={handleBlur}
              onChange={handleChange}
            />
            <ErrorText>{errors.email}</ErrorText>
            <div className="mt-2">contraseña:</div>
            <Input 
              type="password" 
              name="password"
              placeholder="Contraseña"
              value={form.password}
              onBlur={handleBlur}
              onChange={handleChange}
            />
            <ErrorText className="mb-5">{errors.password}</ErrorText>
            
            <Button type="submit" className="justify-center flex">
              { loading ? <Loading/> : 'Entrar' }
            </Button>
          </form>
        </Card>
      </Container>
      {alert ? <Alert close={handleAlert} text="Los datos introducidos son incorrectos, intente nuevamente."></Alert> : ''}
    </>
  );
};