/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from "react";
import Container from "../components/container";
import Title from "../components/title";
import tw from "twin.macro";
import InfoCard from "../components/info";
import Pagination from "../components/pagination";
import axios from "axios";
import Modal from "../components/modal";

const StyledTitle = tw(Title)`text-2xl text-violet-500 ml-5`

const ListContainer = tw(Title)`flex md:flex-row sm:flex-col flex-wrap gap-4 justify-center mt-10`

export default function List() {
  const [count, setCount] = useState(1);
  const [total, setTotal] = useState(0);
  const [list, setList] = useState([]);
  const [listPrev, setlistPrev] = useState(null);
  const [listNext, setlistNext] = useState(null);
  const [pokeInfo, setPokeInfo] = useState({});
  const [showModal, setShowModal] = useState(false);

  const buildPokemonsPayload = async(pokeUrl) => {
    const response = await axios.get(pokeUrl);
    setTotal(response.data.count);
    setlistPrev(response.data.previous);
    setlistNext(response.data.next);
    const pokeArray = response.data.results;
    return pokeArray
  }
  
  const buildPokemonPayload = async (pokeArray) => {
    const newPokemon = pokeArray.map( async(pokemon) => {
      const response = await axios.get(pokemon.url);
      const poke = response.data;
      let movesArray = [];
      poke.moves.every((move) => {
        movesArray.push(move);
        if (movesArray.length === 2) {
          return false;
        }
        return true;
      });
      return {
        id: poke.id,
        name: poke.name,
        weight: poke.weight,
        moves: movesArray,
        img: poke.sprites.other.dream_world.front_default,
        imgAlt: poke.sprites.front_default,
        type: poke.types[0].type.name,
        info: poke
      }
    })
    setList(await Promise.all(newPokemon))
  }

  const getPokemons = async (pokeUrl) =>{
    const pokeList = await buildPokemonsPayload(pokeUrl);
    await buildPokemonPayload(await Promise.all(pokeList));
  }

  useEffect(() => {
    getPokemons("https://pokeapi.co/api/v2/pokemon?limit=10");
    setCount(1);
  }, [])

  function handleNext() {
    if (listNext) {
      getPokemons(listNext);
      setCount(count+10);
    } else {
      getPokemons("https://pokeapi.co/api/v2/pokemon?limit=10");
      setCount(1);
    }
  }
  
  function handlePrev() {
    if (listPrev) {
      getPokemons(listPrev);
      setCount(count-10);
    } else {
      getPokemons('https://pokeapi.co/api/v2/pokemon?offset=1282&limit=10')
      setCount(1283);
    }
  }

  function handleModal (info){
    setPokeInfo(info)
    setShowModal(!showModal)
  }

  return (
    <>
      {showModal ? (
        <Modal handleModal={() => handleModal({})} info={pokeInfo}/>
      ) : null}
      <Container>
        <StyledTitle>Lista de pokémon disponibles:</StyledTitle>
        <Pagination total={total} count={count} next={handleNext} prev={handlePrev} />
        <ListContainer>
          {list.map(poke =>
            <InfoCard
              key={poke.id}
              name={poke.name}
              img={poke.img}
              imgAlt={poke.imgAlt}
              moves={poke.moves || []}
              weight={poke.weight}
              type={poke.type}
              showModal={() => handleModal(poke.info)}
            ></InfoCard>
          )}
        </ListContainer>
        <Pagination total={total} count={count} next={handleNext} prev={handlePrev} />
      </Container>
    </>
  );
}