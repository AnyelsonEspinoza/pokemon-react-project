import { useEffect } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import NavBar from "../components/navbar";

export default function Layout() {
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (!token) {
      navigate("/");
    }
  }, [navigate]);

  return (
    <>
      <div>
        <NavBar />
      </div>
      <div className="mt-20">
        <Outlet />
      </div>
    </>
  );
}