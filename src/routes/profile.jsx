import { useSelector } from "react-redux/es/hooks/useSelector";
import AvatarProfile from "../components/avatarProfile";
import Container from "../components/container";
import tw from "twin.macro";

const Card = tw.div`
  flex flex-col md:w-1/2 mx-auto 
`

const Content = tw.div`
  flex flex-row place-content-between p-3 gap-40 font-semibold text-lg bg-purple-500 mt-4 text-white rounded-3xl
`

export default function Profile() {
  const user = useSelector((state) => state.user)
  return (
    <>
      <Container>
        <Card>
          <AvatarProfile></AvatarProfile>
          <Content>
            <span>Nombre:</span>
            <span>{user.name}</span>
          </Content>
          <Content>
            <span>Apellido:</span>
            <span>{user.lastname}</span>
          </Content>
          <Content>
            <span>Email:</span>
            <span>{user.email}</span>
          </Content>
          <Content>
            <span>Teléfono</span>
            <span>{user.phone}</span>
          </Content>
        </Card>
      </Container>
    </>
  )
}