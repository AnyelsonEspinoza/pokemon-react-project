import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  name: "",
  lastname: "",
  email: "",
  phone: ""
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    addUser: (state, action) => {
      const {name, lastname, email, phone} = action.payload;
      state.name = name;
      state.lastname = lastname;
      state.email = email;
      state.phone = phone;
    }
  }
});

export const { addUser } = userSlice.actions;
export default userSlice.reducer;