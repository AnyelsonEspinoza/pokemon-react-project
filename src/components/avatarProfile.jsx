import tw from 'twin.macro'

const AvatarButton = tw.button`
  mx-auto text-sm rounded-full ring-4 ring-violet-400
`
const AvatarImg = tw.img`w-36 h-36 rounded-full`


export default function AvatarProfile(props) {
  return (
    <AvatarButton onClick={props.menuAction}>
      <AvatarImg src='https://flowbite.com/docs/images/people/profile-picture-5.jpg' alt="user" />
    </AvatarButton>
  )
}