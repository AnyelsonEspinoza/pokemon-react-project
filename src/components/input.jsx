import tw from 'twin.macro';

export const Input = tw.input`
  border-solid border-2 border-indigo-500 rounded-md py-1 pl-3 shadow-lg
`