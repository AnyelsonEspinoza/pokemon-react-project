import tw from 'twin.macro'
import { Link } from "react-router-dom";

export const StyledLink =  tw(Link)`block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100`