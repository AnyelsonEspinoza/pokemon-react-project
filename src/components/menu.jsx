import { StyledLink } from "./link";
import { useNavigate } from "react-router-dom";
import tw from 'twin.macro'

export default function Menu(props) {
  const navigate = useNavigate();

  function handleLogOut() {
    localStorage.removeItem('token');
    navigate("/");
  }

  const Menu = tw.div`
    z-20 absolute right-7 top-12 my-4 text-base list-none bg-gray-100 divide-y-2 divide-violet-500 rounded-lg shadow-lg border-2 border-violet-500
  `

  const Name = tw.span`block text-sm text-gray-900`

  const Email = tw.span`block text-sm  text-gray-500 truncate`

  return (
    <Menu>
      <div className="px-4 py-3">
        <Name>Jhon Doe</Name>
        <Email>test@gmail.com</Email>
      </div>
      <ul className="py-2">
        <li>
          <StyledLink name="dashboard" onClick={props.menu} to={'/dashboard'}>Dashboard</StyledLink>
        </li>
        <li>
          <StyledLink name="profile" to={'/dashboard/profile'}>Perfil</StyledLink>
        </li>
        <li>
          <StyledLink name="logout" onClick={handleLogOut}>Cerrar Sesion</StyledLink>
        </li>
      </ul>
    </Menu>
  ) 
}