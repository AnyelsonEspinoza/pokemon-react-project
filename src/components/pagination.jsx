import tw from "twin.macro"

const Container = tw.div`flex flex-col items-center mt-3`

const Numbers = tw.span`font-semibold text-gray-900 mx-1`

const ButtonNext = tw.div`
  cursor-pointer flex items-center justify-center px-3 h-8 text-sm font-medium text-white bg-purple-800 rounded-s hover:bg-purple-900
`
const ButtonPrev = tw.div`
  cursor-pointer flex items-center justify-center px-3 h-8 text-sm font-medium text-white bg-purple-800 border-0 border-s border-gray-700 rounded-e hover:bg-purple-900
`
const ButtonIcon = tw.svg`
  w-3.5 h-3.5 rtl:rotate-180
`

export default function Pagination(props) {
  return(
    <Container>
      {/* <!-- Help text --> */}
      <span className="text-sm text-gray-700">
          Mostrando del
          <Numbers>{props.count}</Numbers> 
          al
          <Numbers>{props.count + 9}</Numbers> 
          de
          <Numbers>{props.total}</Numbers>
          Entradas
      </span>
      <div className="inline-flex mt-2 xs:mt-0">
        {/* <!-- Buttons --> */}
        <ButtonNext onClick={props.prev}>
            <ButtonIcon className="me-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M13 5H1m0 0 4 4M1 5l4-4"/>
            </ButtonIcon>
            Prev
        </ButtonNext>
        <ButtonPrev onClick={props.next}>
            Next
            <ButtonIcon className="ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
              <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
            </ButtonIcon>
        </ButtonPrev>
      </div>
    </Container>
  )
}