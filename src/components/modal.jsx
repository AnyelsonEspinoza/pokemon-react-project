import { useEffect, useState } from "react";
import tw from "twin.macro";
import colorVariants from "../constants/colorVariants";

const Background = tw.div`
  fixed inset-0 z-50 bg-gray-500 bg-opacity-75 transition-opacity
`
const PokeModal = tw.div`
  flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none
`

const Container = tw.div `
  relative sm:w-full lg:w-1/2 mx-auto sm:mt-20 md:mt-0
`

const Card = tw.div`
  border-0 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none rounded-lg
`

const Bar = tw.div`
  flex items-start justify-between p-3 bg-purple-500 divide-y-reverse divide-black rounded-t-lg
`

const Title = tw.div`
  text-xl text-white font-semibold
`

const DetailsContainer = tw.div`
  flex flex-col p-3 justify-center
`

const DetailsTitle = tw.div`
  font-bold text-lg
`
const DetailsContent = tw.div`
  ml-2 font-semibold 
`

const MovesContainer = tw.div`
  flex flex-row flex-wrap gap-3 h-28 overflow-auto w-96 text-white rounded-lg bg-indigo-100
`

const DataContainer = tw.div`
  sm:rounded-b-lg md:rounded-tl-lg md:rounded-br-lg md:w-3/6 sm:w-full border-l-2 border-t-2 border-b-2 border-black flex flex-col justify-center align-middle
`

const Data = tw.div`
  mx-auto text-white font-semibold text-lg
`

const DataImage = tw.img`
  md:w-1/2 sm:w-20 mx-auto
`

const Button = tw.button`
  h-7 w-7 bg-transparent float-right
`

const ButtonText = tw.span`
  text-white text-2xl
`

const ContentCard = tw.span`
  flex md:flex-row sm:flex-col-reverse justify-between
`

export default function Modal(props) {

  const [info, setInfo] = useState({});

  useEffect(() => {
    setInfo({
      id: props.info.id,
      name: props.info.name,
      weight: props.info.weight,
      img: props.info.sprites.other.dream_world.front_default,
      imgAlt: props.info.sprites.front_default,
      type: props.info.types[0].type.name,
      types: props.info.types,
      abilities: props.info.abilities,
      stats: props.info.stats,
      moves: props.info.moves
    })
  }, [props])

  return (
    <>
      <Background />
      <PokeModal>
        <Container>
          <Card>
            <Bar>
              <Title>Detalles de pokémon</Title>
              <Button name="closeModal" onClick={props.handleModal}>
                <ButtonText>
                  x
                </ButtonText>
              </Button>
            </Bar>
            <ContentCard>
              <DetailsContainer>
                <DetailsTitle>Habilidades: </DetailsTitle>
                {info.abilities? info.abilities.map((abilities, i) =>
                  <DetailsContent key={i+'ability'}>
                    {abilities.ability.name} {abilities.is_hidden? '(Oculta)' : ''}
                  </DetailsContent>
                ): null}
                <DetailsTitle>Estadisticas Base: </DetailsTitle>
                {info.stats? info.stats.map((stat, i) =>
                  <DetailsContent  key={i+'stat'}>
                    {stat.stat.name}: {stat.base_stat}
                  </DetailsContent>
                ): null}
                <DetailsTitle>Movimientos:</DetailsTitle>
                <MovesContainer>
                  {info.moves? info.moves.map((move, i) =>
                    <DetailsContent className={`rounded-lg p-2 ${colorVariants[info.type]}`} key={i+'stat'}>
                      {move.move.name}
                    </DetailsContent>
                  ): null}
                </MovesContainer>
              </DetailsContainer>
              <DataContainer className={`${colorVariants[info.type]}`}>
                <DataImage src={info.img ? info.img : info.imgAlt } alt="Imagen pokemon" />
                <Data>{info.name} #{info.id}</Data>
                <Data>
                  {info.types? info.types.map(type => 
                    <span key={type.type.name} className="ml-1">#{type.type.name}</span>
                  ): null}  
                </Data>
                <Data>{info.weight}lbs</Data>
              </DataContainer>
            </ContentCard>
          </Card>
        </Container>
      </PokeModal>
    </>
  );
};