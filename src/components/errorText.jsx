import tw from 'twin.macro';

const ErrorText = tw.p`text-red-400 text-sm ml-1`

export default ErrorText;