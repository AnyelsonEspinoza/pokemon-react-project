import  tw from 'twin.macro';

const Container = tw.div`w-11/12 p-10 bg-gray-100 mt-2 mx-auto rounded-lg`

export default Container;