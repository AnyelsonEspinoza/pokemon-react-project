import tw from 'twin.macro'
import colorVariants from '../constants/colorVariants'

const Card = tw.div`
  lg:w-1/5 md:w-1/4 sm:w-64 sm:mx-auto md:mx-1 cursor-pointer relative rounded-2xl overflow-hidden shadow-lg transition-all  hover:-translate-y-12 hover:shadow-2xl pt-2 flex flex-col border-black border-2
`
const Tag = tw.span`inline-block rounded-full px-3 py-1 text-sm font-semibold text-white mr-2 mb-2 border-black border-2 absolute right-0 top-1/2`

const Title = tw.span`font-bold text-xl mb-2`

const Image = tw.img`md:h-40 w-1/2 mx-auto`

const Moves = tw.span`mt-3 m-4 flex flex-col font-bold text-sm`

export default function InfoCard(props) {
  const conversor = (lb) => {
    const weight = lb / 2.2;
    return Number.parseFloat(weight).toFixed(2);;
  }
  return (
    <Card name={props.name} onClick={props.showModal} className={`${colorVariants[props.type]}`}>
      <Image className="" src={props.img ? props.img : props.imgAlt } alt="Imagen pokemon" />
      <Tag className={`${colorVariants[props.type]}`}>{conversor(props.weight)}kg</Tag>
      <div className='bg-gray-200'>
        <div className="px-6 py-4">
          <Title>{props.name}</Title>
        </div>
        <Moves>
          {props.moves.map((move, i)=> {
            return <div key={i} className='ml-1'>#{move.move.name}</div>
          })}
        </Moves>
      </div>
    </Card>
  )
} 