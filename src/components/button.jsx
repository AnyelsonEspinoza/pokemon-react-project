import tw from 'twin.macro';

export const Button = tw.button`
  bg-gradient-to-r from-purple-500 to-purple-300 rounded py-2 shadow-lg text-white font-bold
`