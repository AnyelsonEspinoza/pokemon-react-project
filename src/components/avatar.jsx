import tw from 'twin.macro'

const AvatarButton = tw.button`
  flex text-sm rounded-full md:me-0 ring-4 ring-violet-400 hover:ring-white
`
const AvatarImg = tw.img`w-10 h-10 rounded-full`


export default function Avatar(props) {
  return (
    <AvatarButton name="avatar" onClick={props.menuAction}>
      <AvatarImg src='https://flowbite.com/docs/images/people/profile-picture-5.jpg' alt="user" />
    </AvatarButton>
  )
}