import { useState } from 'react';
import { Link } from "react-router-dom";
import tw from 'twin.macro'
import Menu from "./menu";
import Avatar from './avatar';

const NavStyle = tw.nav`bg-purple-500 fixed top-0 w-screen z-50`

const NavContainer = tw.div`max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4`

const NavTitle = tw.div`self-center text-2xl font-semibold whitespace-nowrap text-white`

const NavMenu = tw.div`flex items-center md:order-2 space-x-3 md:space-x-0 rtl:space-x-reverse`

export default function NavBar() {
  const [showMenu, setShowMenu] = useState(false);

  function handleShowMenu() {
    setShowMenu(!showMenu);
  }
  
  return (
    <NavStyle>
      <NavContainer className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <Link to={`/dashboard`} href="https://flowbite.com/" className="flex items-center space-x-3 rtl:space-x-reverse">
            <img src={require('../assets/Master-Ball.png')} className="h-8" alt="Logo" />
            <NavTitle>MonomaDex</NavTitle>
        </Link>
        <NavMenu>
          <Avatar menuAction={handleShowMenu}/>
          {showMenu? <Menu /> : ''}
        </NavMenu>
      </NavContainer>
    </NavStyle>
  )
}