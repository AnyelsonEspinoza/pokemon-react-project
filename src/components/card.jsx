import  tw from 'twin.macro';

const Card = tw.div`md:w-1/2 sm:w-full p-10 bg-white border shadow-xl flex flex-col justify-center`

export default Card;