const colorVariants = {
  grass: 'bg-grass',
  poison: 'bg-poison',
  dark: 'bg-dark',
  ghost: 'bg-ghost',
  fire: 'bg-fire',
  water: 'bg-water',
  psychic: 'bg-psychic',
  dragon: 'bg-dragon',
  fairy: 'bg-fairy',
  electric: 'bg-electric',
  ground: 'bg-ground',
  rock: 'bg-rock',
  steel: 'bg-steel',
  bug: 'bg-bug',
  fighting: 'bg-fighting',
  flying: 'bg-flying',
  normal: 'bg-normal',
  ice: 'bg-ice',
  unknown: 'bg-unknown',
  shadow: 'bg-shadow',
}

export default colorVariants;