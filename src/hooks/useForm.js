import { useState } from "react";

export const useForm = (initialForm, validateForm, handleLogin) => {
  const [form, setForm] = useState(initialForm);
  const [errors, setErrors] = useState({});
  const [alert, setAlert] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleChange = (e) => {
    const {name, value} = e.target;
    setForm({
      ...form,
      [name]: value
    });
  };

  const handleBlur = (e) => {
    handleChange(e);
    setErrors(validateForm(form));
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const errors = validateForm(form)
    setErrors(errors);
    if (Object.keys(errors).length === 0) {
      handleLogin();
    }
  };

  const handleAlert = () => {
    setAlert(true);
    
    setTimeout(() => {
      setAlert(false);
    }, 3000);
  }

  const handleLoading = (data) => {
    setLoading(data);
  }

  return {
    form, 
    errors,
    alert,
    loading,
    handleChange,
    handleBlur,
    handleSubmit,
    handleAlert,
    handleLoading
  }
}